package us.AWPTO.Statki;
import com.awpto.statki.ui.GameFrame;

/*
 * Co w javie piszczy, czyli co robi� poszczegolne klasy:
 * 
 * 	PACKAGE .statki:
 * 
 * 	ShipBase - klasa bazowa do reprezentacji statk�w
 * 	ShipPrototype - klasa reprezentuj�ca statki w trakcie ustawiania ich na planszy
 * 					ma pare metod, kt�re nie s� wykorzystywane - s� bo s� :p
 * 					metoda build() tworzy obiekt typu Ship na podstawie ShipPrototype
 * 	Ship - klasa reprezentuj�ca statki podczas gry
 * 	
 * 	FormationBase - klasa bazowa do reprezentacji planszy/floty/jak zwa� tak zwa�
 * 	FormationPrototype - klasa reprezentuj�ca flot� w trakcie ustawiania w niej statk�w
 * 						 metoda build() tworzy obiekt typu Formation na podstawie FormationPrototype
 * 	Formation - klasa reprezentuj�ca flot� podczas gry
 * 				dodatkowo, zawiera macierz typu HitInfo zawieraj�c� informacje, czy flota zosta�a ju� w tym punkcie zaatakowana
 * 				i, je�eli tak, to jaki by� tego efekt
 *  HitInfo - Formation zawiera macierz takich enum'�w, reprezentuje info o stanie bycia zaatakowanym danego punktu floty
 *  AttackResult - enum zwracany po wykonaniu ataku na jakim� Formation (metody getHitAt), informuje o tym czy atak si� powi�d� czy nie, itp
 *  			   zwracany r�wnie� jako wynik ataku gracza (PlayerBase - metoda performAttack())
 * 
 * 	FleetComposer - klasa pozwalaj�ca na dodawanie statk�w do planszy/floty w cywilizowany sposob
 * 					do konstruktora podajemy rozmiary statk�w i ile ich chcemy oraz rozmiary planszy
 *  NextShipResult - ka�da pr�ba dodania nowego statku za pomoc� FleetComposer'a zwraca ten enum
 * 
 * 	RandomFormationCreator - klasa generuj�ca losowo rozmieszczone statki na planszy na podstawie jakiego� FleetComposer'a
 * 							 dla AI
 * 
 * 	PlayerBase - klasa bazowa do reprezentacji gracza, zawiera referencje do floty gracza i floty przeciwnika
 * 	HumanPlayer - klasa reprezentuj�ca gracza-ludzia, posiada dwie statyczne wsp�rz�dne targetX i targetY
 * 				  kt�re ustawiamy przed wykonaniem ataku
 *	AIRandomPlayer - klasa reprezentuj�ca gracza AI, kt�ry wybiera atakowane punkty kompletnie losowo
 *	AISmarterPlayer - klasa reprezentuj�ca gracza AI, kt�ry sprawdza kilka rzeczy przed wykonaniem ataku
 *					  w skr�cie, pami�ta pozycje statk�w, kt�re zaatakowa� ale jeszcze nie zniszczy� i b�dzie pr�bowa� zniszczyc je w 1. kolejno�ci
 *					  je�eli nie ma informacji o �adnym statku to atakuje losowo
 *
 * 	GameTurnManager - klasa kontroluj�ca rozgrywk� turow� mi�dzy dwoma dowolnymi graczami
 * 	TurnResult - enum zwracany po wykonaniu ka�dej tury za pomoc� GameTurnManager'a, informuje o tym, co si� podczas tej tury wydarzy�o
 * 
 * 
 * 
 *	PACKAGE .statki.ui:
 *
 *	FormationPanel - pozwala na rysowanie obiekt�w typu FormationBase, mo�na wybra� czy chcemy rysowa� bloki statk�w i/lub dane z macierzy HitInfo
 *					 zawiera 'delegate' onClick typu ITileClickedHandler - jest on pod��czony do eventu MousePressed
 *					 przy czym do jego 'EventArgs' przesy�ana jest pozycja klikni�tego bloku, a nie (x, y) piksela
 *
 *	GameFrame - okienko aplikacji, odpowiedzialne za aktualizowanie ui i ustawianie/resetowanie gry
 *
 *
 *
 *	PACKAGE .statki.delegates:
 *
 *	GameCreator - klasa 'statyczna' pozwalaj�ca na generowanie nowej gry (GameTurnManager) na podstawie tego co sobie wyklikamy (+ tworzy przeciwnika ai)
 *				  i na generowanie default FleetComposer'a
 *
 *	ITileClickHandler - interfejs, kt�ry chce by� c# delegat�. mo�e by� invokowany
 *
 *	HumanShipDeployer - implementuje naszego delegate, przetrawia klikanie na FormationPanel i pr�buje dodawa� kolejne statki do floty
 *						korzystaj�c z FleetComposer'a, a wynik tych pr�b przesy�a do ui (GameFrame)
 *
 *	HumanAttackHandler - implementuje naszego delegate, przetrawia klikanie na FormationPanel i pr�buje atakowa� przeciwnika (tylko podczas tury ludzia)
 *						 korzystaj�c z GameTurnManager'a, a wynik tych pr�b przesy�a do ui (GameFrame)
 *
 *	NextAiTurnHandler - przetrawia klikanie w przycisk Next AI Turn znajduj�cy si� w GameFrame, przetrawia pojedyncz� tur� ai
 *						korzystaj�c z GameTurnManager'a i wynik tej tury przesy�a do ui (GameFrame)
 *
 *
 * 	gui jest brzydkie bo si� nie znam na javie/swingu ;(
 */

public class Program
{
	public static void main(String[] args)
	{
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{   			  
				new GameFrame("Statki");
			}
		});
	}
}
