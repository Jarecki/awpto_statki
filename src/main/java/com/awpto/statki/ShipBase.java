package com.awpto.statki;

import java.awt.Point;
import java.awt.Rectangle;

public abstract class ShipBase
{
	protected Point location;
	protected boolean horizontal;
	
	protected ShipBase()
	{ }
	
	protected ShipBase(int x, int y)
	{
		location = new Point(x, y);
		horizontal = true;
	}
	
	public Point getLocation()
	{
		return new Point(location);
	}
	
	public int getTop()
	{
		return location.y;
	}
	
	public int getBottom()
	{
		return horizontal ? location.y : (location.y + getLength() - 1);
	}
	
	public int getLeft()
	{
		return location.x;
	}
	
	public int getRight()
	{
		return horizontal ? (location.x + getLength() - 1) : location.x;
	}
	
	public boolean isHorizontal()
	{
		return horizontal;
	}
	
	public int getX()
	{
		return location.x;
	}
	
	public int getY()
	{
		return location.y;
	}
	
	public abstract int getLength();
	public abstract boolean isDamagedAt(int index);
	
	public int getWidth()
	{
		return horizontal ? getLength() : 1;
	}
	
	public int getHeight()
	{
		return horizontal ? 1 : getLength();
	}
	
	public Rectangle getBounds()
	{
		return new Rectangle(getLeft(), getTop(), getWidth(), getHeight());
	}
	
	public boolean contains(Point p)
	{
		return contains(p.x, p.y);
	}
	
	public boolean contains(int x, int y)
	{
		return getBounds().contains(x, y);
	}
}
