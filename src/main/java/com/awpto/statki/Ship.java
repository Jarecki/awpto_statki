package com.awpto.statki;

import java.awt.Point;

public class Ship extends ShipBase
{
	private boolean[] damaged;
	
	public Ship(ShipPrototype prototype)
	{
		super();
		
		if(prototype == null)
			throw new NullPointerException("prototype");
		
		location = prototype.getLocation();
		horizontal = prototype.isHorizontal();

		damaged = new boolean[prototype.getLength()];
		for(int i = 0; i < damaged.length; ++i)
			damaged[i] = false;
	}
	
	public int getLength()
	{
		return damaged.length;
	}
	
	public int getIndexAt(Point location)
	{
		return getIndexAt(location.x, location.y);
	}
	
	public int getIndexAt(int x, int y)
	{
		if(!contains(x, y)) return -1;
		return horizontal ? (x - location.x) : (y - location.y);
	}
	
	public void damageAt(int index)
	{
		damaged[index] = true;
	}
	
	public boolean tryDamageAt(Point location)
	{
		return tryDamageAt(location.x, location.y);
	}
	
	public boolean tryDamageAt(int x, int y)
	{
		int index = getIndexAt(x, y);
		if(index != -1)
		{
			damageAt(index);
			return true;
		}
		return false;
	}
	
	public boolean isDamagedAt(int index)
	{
		return damaged[index];
	}
	
	public boolean isDamagedAt(Point location)
	{
		return isDamagedAt(location.x, location.y);
	}
	
	public boolean isDamagedAt(int x, int y)
	{
		int index = getIndexAt(x, y);
		return (index != -1) ? isDamagedAt(index) : false;
	}
	
	public int getHealth()
	{
		int count = 0;
		for(int i = 0; i < damaged.length; ++i)
			if(!damaged[i]) ++count;
		return count;
	}
	
	public boolean isDestroyed()
	{
		for(int i = 0; i < damaged.length; ++i)
			if(!damaged[i]) return false;
		return true;
	}
}
