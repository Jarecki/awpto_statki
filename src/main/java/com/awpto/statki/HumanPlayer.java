package com.awpto.statki;

public class HumanPlayer extends PlayerBase
{
	public static int targetX = 0;
	public static int targetY = 0;
	
	public HumanPlayer(Formation formation, Formation enemyFormation)
	{
		super(formation, enemyFormation);
	}

	public boolean isAi()
	{
		return false;
	}

	public AttackResult performAttack()
	{
		return enemyFormation.getHitAt(targetX, targetY);
	}

}
