package com.awpto.statki;

public class GameTurnManager
{
	private boolean gameEnded;
	private boolean player1Turn;
	private PlayerBase current;
	private PlayerBase other;
	
	public GameTurnManager(PlayerBase player1, PlayerBase player2)
	{
		if(player1 == null || player2 == null)
			throw new NullPointerException("player1 || player2");
		
		if(player1.formation != player2.enemyFormation || player2.formation != player1.enemyFormation || player1.formation == player2.formation)
			throw new RuntimeException("players/enemies are not set up correctly.");
		
		gameEnded = false;
		player1Turn = true;
		current = player1;
		other = player2;
	}
	
	public boolean isPlayer1Turn()
	{
		return player1Turn;
	}
	
	public boolean isGameEnded()
	{
		return gameEnded;
	}
	
	public boolean hasPlayer1Won()
	{
		return (getPlayer2().formation.shipsAlive() == 0);
	}
	
	public boolean hasPlayer2Won()
	{
		return (getPlayer1().formation.shipsAlive() == 0);
	}
	
	public PlayerBase getPlayer1()
	{
		return player1Turn ? current : other;
	}
	
	public PlayerBase getPlayer2()
	{
		return player1Turn ? other : current;
	}
	
	public PlayerBase getCurrent()
	{
		return current;
	}
	
	public PlayerBase getOther()
	{
		return other;
	}
	
	public TurnResult nextTurn()
	{
		if(!gameEnded)
		{
			AttackResult result = current.performAttack();
			
			if(result == AttackResult.AlreadyPerformed)
				return TurnResult.WrongInput;
			
			if(result == AttackResult.Missed)
			{
				switchPlayers();
				return TurnResult.PlayersSwitched;
			}
			
			if(result == AttackResult.Hit)
				return TurnResult.PlayerContinues;

			gameEnded = (other.formation.shipsAlive() == 0);
			if(!gameEnded) return TurnResult.PlayerDestroyedShip;
		}
		return TurnResult.GameEnded;
	}
	
	private void switchPlayers()
	{
		player1Turn = !player1Turn;
		PlayerBase p = current;
		current = other;
		other = p;
	}
}
