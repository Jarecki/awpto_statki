package com.awpto.statki;

public enum TurnResult
{
	WrongInput,
	PlayersSwitched,
	PlayerContinues,
	PlayerDestroyedShip,
	GameEnded
}
