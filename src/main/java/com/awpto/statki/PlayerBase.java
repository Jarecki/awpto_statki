package com.awpto.statki;

public abstract class PlayerBase
{
	public final Formation formation;
	public final Formation enemyFormation;
	
	protected PlayerBase(Formation formation, Formation enemyFormation)
	{
		if(formation == null || enemyFormation == null)
			throw new NullPointerException("formation || enemyFormation");
		
		if(formation == enemyFormation)
			throw new RuntimeException("formation and enemyFormation must be different objects.");
		
		this.formation = formation;
		this.enemyFormation = enemyFormation;
	}
	
	public abstract boolean isAi();
	public abstract AttackResult performAttack();
}
