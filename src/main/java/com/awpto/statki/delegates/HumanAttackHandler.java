package com.awpto.statki.delegates;

import com.awpto.statki.GameTurnManager;
import com.awpto.statki.HumanPlayer;
import com.awpto.statki.ui.FormationPanel;
import com.awpto.statki.ui.GameFrame;

public class HumanAttackHandler implements ITileClickedHandler
{
	private GameFrame ui;
	public final GameTurnManager manager;
	
	public HumanAttackHandler(GameFrame ui, GameTurnManager manager)
	{
		if(ui == null || manager == null)
			throw new NullPointerException("ui || manager");
		
		this.ui = ui;
		this.manager = manager;
	}

	public void invoke(FormationPanel sender, int x, int y)
	{
		if(!manager.getCurrent().isAi())
		{
			HumanPlayer.targetX = x;
			HumanPlayer.targetY = y;
			ui.updateNextTurn(manager, manager.nextTurn());
		}
	}
}
