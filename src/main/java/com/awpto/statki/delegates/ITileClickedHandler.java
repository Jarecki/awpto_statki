package com.awpto.statki.delegates;

import com.awpto.statki.ui.FormationPanel;

public interface ITileClickedHandler
{
	void invoke(FormationPanel sender, int x, int y);
}
