package com.awpto.statki.delegates;

import com.awpto.statki.FleetComposer;
import com.awpto.statki.ui.FormationPanel;
import com.awpto.statki.ui.GameFrame;

public class HumanShipDeployer implements ITileClickedHandler
{
	public final FleetComposer composer;
	private GameFrame ui;
	
	public HumanShipDeployer(GameFrame ui, FleetComposer composer)
	{
		if(ui == null || composer == null)
			throw new NullPointerException("ui || composer");
		
		this.ui = ui;
		this.composer = composer;
		composer.reset();
	}

	public void invoke(FormationPanel sender, int x, int y)
	{
		if(sender.formation == composer.formation)
		{
			ui.updateComposingFleet(this, composer.AddShipAt(x, y, ui.isHorizontalChecked()));
		}
		else throw new RuntimeException("sender's formation and deployer's formation must be the same object.");
	}
}
