package com.awpto.statki.delegates;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import com.awpto.statki.GameTurnManager;
import com.awpto.statki.ui.GameFrame;

public class NextAiTurnHandler implements MouseListener
{
	private JButton button;
	private GameFrame ui;
	public GameTurnManager manager;
	
	public NextAiTurnHandler(JButton button, GameFrame ui)
	{
		if(button == null || ui == null)
			throw new NullPointerException("button || ui");
		
		this.button = button;
		this.ui = ui;
	}

	public void mousePressed(MouseEvent e)
	{
		if(button == e.getSource() && manager != null && manager.getCurrent().isAi())
		{
			ui.updateNextTurn(manager, manager.nextTurn());
		}
	}

	public void mouseClicked(MouseEvent e) { }
	public void mouseEntered(MouseEvent e) { }
	public void mouseExited(MouseEvent e) { }
	public void mouseReleased(MouseEvent e) { }
}
