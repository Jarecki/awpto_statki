package com.awpto.statki;

import java.awt.Point;

public class Formation extends FormationBase
{
	private Ship[] ships;
	private HitInfo[] hitInfo;
	
	public Formation(FormationPrototype prototype)
	{
		super();
		
		if(prototype == null)
			throw new NullPointerException("prototype");
		
		columns = prototype.getColumns();
		rows = prototype.getRows();
		
		hitInfo = new HitInfo[columns * rows];
		for(int i = 0; i < hitInfo.length; ++i)
			hitInfo[i] = HitInfo.None;
		
		ships = new Ship[prototype.getShipCount()];
		for(int i = 0; i < ships.length; ++i)
			ships[i] = prototype.at(i).build();
	}
	
	public int getShipCount()
	{
		return ships.length;
	}
	
	public Ship at(int index)
	{
		return ships[index];
	}
	
	public Ship at(Point location)
	{
		return at(location.x, location.y);
	}
	
	public Ship at(int x, int y)
	{
		for(int i = 0; i < ships.length; ++i)
			if(ships[i].contains(x, y)) return ships[i];
		return null;
	}
	
	public int getTotalHealth()
	{
		int health = 0;
		for(int i = 0; i < ships.length; ++i)
			health += ships[i].getHealth();
		return health;
	}
	
	public int shipsAlive()
	{
		int count = 0;
		for(int i = 0; i < ships.length; ++i)
			if(!ships[i].isDestroyed()) ++count;
		return count;
	}
	
	public HitInfo isHitAt(Point location)
	{
		return isHitAt(location.x, location.y);
	}
	
	public HitInfo isHitAt(int x, int y)
	{
		return hitInfo[translIndex(x, y)];
	}
	
	public AttackResult getHitAt(Point location)
	{
		return getHitAt(location.x, location.y);
	}
	
	public AttackResult getHitAt(int x, int y)
	{
		int index = translIndex(x, y);
		if(hitInfo[index] != HitInfo.None) return AttackResult.AlreadyPerformed;
		
		Ship ship = at(x, y);
		if(ship == null || !ship.tryDamageAt(x, y))
		{
			hitInfo[index] = HitInfo.NotHit;
			return AttackResult.Missed;
		}
		hitInfo[index] = HitInfo.Hit;
		return ship.isDestroyed() ? AttackResult.HitAndDestroyed : AttackResult.Hit;
	}
	
	private int translIndex(int x, int y)
	{
		return x + y * columns;
	}
}
