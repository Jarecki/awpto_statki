package com.awpto.statki.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import com.awpto.statki.FormationBase;
import com.awpto.statki.ShipBase;
import com.awpto.statki.delegates.ITileClickedHandler;
import com.awpto.statki.HitInfo;

@SuppressWarnings("serial")
public class FormationPanel extends JPanel implements MouseListener
{
	public ITileClickedHandler onClick;
	public FormationBase formation;
	public boolean drawHitInfo;
	public boolean drawShips;
	private int tileSize;
	
	public FormationPanel()
	{
		super(true);
		initialize();
		tileSize = 45;
		drawHitInfo = false;
		drawShips = true;
	}
	
	public boolean hasFormation()
	{
		return (formation != null);
	}
	
	public int getTileSize()
	{
		return tileSize;
	}
	
	public void setTileSize(int value)
	{
		tileSize = (value < 15) ? 15 : value;
	}
	
	public Dimension getFormationSize()
	{
        return hasFormation() ? new Dimension(formation.getColumns() * tileSize + 1, formation.getRows() * tileSize + 1) :
        						new Dimension(1, 1);
	}

	public void mousePressed(MouseEvent arg0)
	{
		if(onClick != null)
			onClick.invoke(this, arg0.getX() / tileSize, arg0.getY() / tileSize);
	}
	
	public void mouseClicked(MouseEvent arg0) { }
	public void mouseEntered(MouseEvent arg0) { }
	public void mouseExited(MouseEvent arg0) { }
	public void mouseReleased(MouseEvent arg0) { }
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if(hasFormation())
		{
			drawGrid(g, formation.getColumns(), formation.getRows());
			if(drawShips) drawFormation(g);
			if(drawHitInfo) drawHits(g);
		}
	}
	
	private void drawFormation(Graphics g)
	{
		int ships = formation.getShipCount();
		for(int i = 0; i < ships; ++i)
		{
			ShipBase ship = formation.at(i);
			if(ship.isHorizontal()) drawShipHorizontal(g, ship);
			else drawShipVertical(g, ship);
		}
	}
	
	private void drawGrid(Graphics g, int columns, int rows)
	{
		g.setColor(Color.BLACK);
		
		for(int i = 0; i < columns; ++i)
		{
			int x = i * tileSize;
			g.drawLine(x, 0, x + tileSize, 0);
		}
		
		for(int i = 0; i < rows; ++i)
		{
			int y = i * tileSize;
			g.drawLine(0, y, 0, y + tileSize);
		}

		for(int i = 0; i < columns; ++i)
		{
			for(int j = 0; j < rows; ++j)
			{
				int x0 = i * tileSize;
				int y0 = j * tileSize;
				int x1 = x0 + tileSize;
				int y1 = y0 + tileSize;
				g.drawLine(x1, y0, x1, y1);
				g.drawLine(x0, y1, x1, y1);
			}
		}
	}
	
	private void drawShipHorizontal(Graphics g, ShipBase ship)
	{
		int y = ship.getY();
		int x = ship.getX();
		int length = ship.getLength();
		
		for(int i = 0; i < length; ++i, ++x)
		{
			g.setColor(ship.isDamagedAt(i) ? Color.RED : Color.ORANGE);
			drawShipTile(g, x, y);
		}
	}
	
	private void drawShipVertical(Graphics g, ShipBase ship)
	{
		int y = ship.getY();
		int x = ship.getX();
		int length = ship.getLength();
		
		for(int i = 0; i < length; ++i, ++y)
		{
			g.setColor(ship.isDamagedAt(i) ? Color.RED : Color.ORANGE);
			drawShipTile(g, x, y);
		}
	}
	
	private void drawShipTile(Graphics g, int x, int y)
	{
		g.fillRect(x * tileSize + 1, y * tileSize + 1, tileSize - 1, tileSize - 1);
	}
	
	private void drawHits(Graphics g)
	{
		g.setColor(Color.BLACK);
		
		int columns = formation.getColumns();
		int rows = formation.getRows();
		int dotPos = (tileSize >> 1) - 3;
		
		for(int x = 0; x < columns; ++x)
		{
			for(int y = 0; y < rows; ++y)
			{
				HitInfo info = formation.isHitAt(x, y);
				if(info != HitInfo.None)
				{
					if(info == HitInfo.Hit)
					{
						int left = x * tileSize + 5;
						int right = x * tileSize + tileSize - 5;
						int top = y * tileSize + 5;
						int bottom = y * tileSize + tileSize - 5;
						g.drawLine(left, top, right, bottom);
						g.drawLine(left, bottom, right, top);
					}
					else g.fillOval(x * tileSize + dotPos, y * tileSize + dotPos, 7, 7);
				}
			}
		}
	}
	
	private void initialize()
	{
		setBackground(Color.WHITE);
		addMouseListener(this);
	}
}
