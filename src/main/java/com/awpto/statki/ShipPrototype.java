package com.awpto.statki;

import java.awt.Point;

public class ShipPrototype extends ShipBase
{
	private int length;
	
	public ShipPrototype()
	{
		super(0, 0);
		length = 1;
	}
	
	public boolean isDamagedAt(int index)
	{
		return false;
	}
	
	public void setOrientation(boolean horizontal)
	{
		this.horizontal = horizontal;
	}
	
	public void setX(int value)
	{
		location.x = value;
	}
	
	public void setY(int value)
	{
		location.y = value;
	}
	
	public void setLocation(Point value)
	{
		if(value != null)
		{
			setX(value.x);
			setY(value.y);
		}
	}
	
	public void setLocation(int x, int y)
	{
		setX(x);
		setY(y);
	}
	
	public int getLength()
	{
		return length;
	}
	
	public void setLength(int value)
	{
		length = (value < 1) ? 1 : value;
	}
	
	public Ship build()
	{
		return new Ship(this);
	}
	
	public boolean collidesWith(ShipPrototype other)
	{
		return getBounds().intersects(other.getBounds());
	}
}
