package com.awpto.statki;

import java.awt.Point;
import java.util.Random;

public class AIRandomPlayer extends PlayerBase
{
	protected static Random rng = new Random();
	
	public AIRandomPlayer(Formation formation, Formation enemyFormation)
	{
		super(formation, enemyFormation);
	}

	public boolean isAi()
	{
		return true;
	}
	
	public AttackResult performAttack()
	{
		return enemyFormation.getHitAt(randomTarget());
	}
	
	protected Point randomTarget()
	{
		int x = rng.nextInt(enemyFormation.getColumns());
		int y = rng.nextInt(enemyFormation.getRows());

		if(enemyFormation.isHitAt(x, y) != HitInfo.None)
		{
			int p0 = x + y * enemyFormation.getColumns();
			int p = p0 + 1;
			
			while(p != p0)
			{
				if(p == enemyFormation.getSize()) p = 0;
				y = p / enemyFormation.getColumns();
				x = p - y * enemyFormation.getColumns();
				if(enemyFormation.isHitAt(x, y) == HitInfo.None)
					return new Point(x, y);
				++p;
			}
			return null;
		}
		return new Point(x, y);
	}
}
