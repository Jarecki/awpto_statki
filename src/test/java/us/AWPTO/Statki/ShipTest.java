package us.AWPTO.Statki;

import static org.junit.Assert.*;

import org.junit.Test;

import com.awpto.statki.Ship;
import com.awpto.statki.ShipPrototype;

public class ShipTest {

	@Test
	public void testDamagedPos() {
		Ship ship = new Ship(new ShipPrototype());
		ship.damageAt(0);
		assertTrue(ship.isDamagedAt(0));
	}

	@Test
	public void testDamagedNeg() {
		Ship ship = new Ship(new ShipPrototype());
		assertFalse(ship.isDamagedAt(0));
	}
	
	@Test
	public void testLenght() {
		Ship ship = new Ship(new ShipPrototype());
		assertNotNull(ship.getLength());
	}
}
