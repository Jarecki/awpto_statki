package us.AWPTO.Statki;

import static org.junit.Assert.*;

import org.junit.Test;

import com.awpto.statki.FleetComposer;

public class FleetComposerTest {

	@SuppressWarnings("unused")
	@Test
	public void testConstructor_nullShipLengths() {
		FleetComposer composer = null;
		try {
			composer = new FleetComposer(null, 10, 10);
		}
		catch(NullPointerException e) {
			return;
		}
		fail("exception wasn't thrown.");
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testConstructor_shipLengthEmpty() {
		FleetComposer composer = null;
		try {
			composer = new FleetComposer(new int[0], 10, 10);
		}
		catch(RuntimeException e) {
			assertEquals("ship count must be greater than 0.", e.getMessage());
			return;
		}
		fail("exception wasn't thrown.");
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testConstructor_fleetTooLarge() {
		FleetComposer composer = null;
		try {
			composer = new FleetComposer(new int[]{ 2, 2, 2, 2, 2 }, 3, 3);
		}
		catch(RuntimeException e) {
			assertEquals("fleet is too large.", e.getMessage());
			return;
		}
		fail("exception wasn't thrown.");
	}

	@Test
	public void testCurrentShipLen() {
		int[] ships = {6,2,3,4};
		FleetComposer fleetComposer = new FleetComposer(ships, 10, 10);
		assertEquals(fleetComposer.getCurrentShipLength(), 6);
	}
	
	@Test
	public void testShipCount() {
		int[] ships = {1,2,3,4};
		FleetComposer fleetComposer = new FleetComposer(ships, 10, 10);
		assertEquals(fleetComposer.getShipCount(), 4);
	}
	
	@Test
	public void testShipPointer() {
		int[] ships = {1,2,3,4};
		FleetComposer fleetComposer = new FleetComposer(ships, 10, 10);
		assertEquals(fleetComposer.getShipPointer(), 0);
	}

	
	@Test
	public void testConstructor_columns() {
		int expected = 10;
		FleetComposer composer = new FleetComposer(new int[]{ 1, 2 }, expected, 8);
		int actual = composer.formation.getColumns();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testConstructor_columnsLessThanOne() {
		FleetComposer composer = new FleetComposer(new int[]{ 1, 2 }, -1, 8);
		int actual = composer.formation.getColumns();
		assertEquals(1, actual);
	}
	
	@Test
	public void testConstructor_rows() {
		int expected = 10;
		FleetComposer composer = new FleetComposer(new int[]{ 1, 2 }, 8, expected);
		int actual = composer.formation.getRows();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testConstructor_rowsLessThanOne() {
		FleetComposer composer = new FleetComposer(new int[]{ 1, 2 }, 8, -1);
		int actual = composer.formation.getRows();
		assertEquals(1, actual);
	}
	
	@Test
	public void testGetShipCount() {
		FleetComposer composer = new FleetComposer(new int[]{ 1, 2, 3, 4 }, 10, 10);
		assertEquals(composer.getShipCount(), 4);
	}

	@Test
	public void testGetShipPointer_init() {
		FleetComposer composer = new FleetComposer(new int[]{ 1, 2, 3, 4 }, 10, 10);
		assertEquals(composer.getShipPointer(), 0);
	}

	@Test
	public void testGetCurrentShipLength_init() {
		FleetComposer composer = new FleetComposer(new int[]{ 1, 2, 3, 4 }, 10, 10);
		assertEquals(composer.getCurrentShipLength(), 1);
	}

	@Test
	public void testIsDone() {
		FleetComposer composer = new FleetComposer(new int[]{ 1, 2 }, 10, 10);
		
		boolean actual1 = composer.isDone();
		composer.AddShipAt(0, 0, true);
		boolean actual2 = composer.isDone();
		composer.AddShipAt(1, 0, true);
		boolean actual3 = composer.isDone();
		
		assertFalse(actual1);
		assertFalse(actual2);
		assertTrue(actual3);
	}

}
