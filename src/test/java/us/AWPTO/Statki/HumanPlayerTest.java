package us.AWPTO.Statki;

import static org.junit.Assert.*;

import org.junit.Test;

import com.awpto.statki.Formation;
import com.awpto.statki.FormationPrototype;
import com.awpto.statki.HumanPlayer;

public class HumanPlayerTest {

	@Test
	public void test() {
		HumanPlayer humanPlayer = new HumanPlayer(new Formation(new FormationPrototype(10,10)), new Formation(new FormationPrototype(10,10)));
		boolean result = humanPlayer.isAi();
		assertFalse(result);
	}

}
