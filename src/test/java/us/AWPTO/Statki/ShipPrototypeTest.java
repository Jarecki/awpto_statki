package us.AWPTO.Statki;

import static org.junit.Assert.*;

import java.awt.Point;
import java.awt.Rectangle;

import org.junit.Before;
import org.junit.Test;

import com.awpto.statki.ShipPrototype;

public class ShipPrototypeTest {

	private ShipPrototype prototype;
	
	@Before
	public void setUp() throws Exception {
		prototype = new ShipPrototype();
	}

	@Test
	public void testIsDamagedAt() {
		assertFalse(prototype.isDamagedAt(0));
		assertFalse(prototype.isDamagedAt(prototype.getLength()));
	}
	
	@Test
	public void testBuild_null() {
		assertNotNull(prototype.build());
	}

	@Test
	public void testBuild_reference() {
		assertNotSame(prototype.build(), prototype.build());
	}

	@Test
	public void testCollidesWith() {
		ShipPrototype other = new ShipPrototype();
		prototype.setLocation(10, 15);
		prototype.setLength(4);
		other.setLocation(10, 15);
		other.setLength(2);
		
		boolean actual1 = prototype.collidesWith(other);
		other.setLocation(11, 15);
		boolean actual2 = prototype.collidesWith(other);
		other.setLocation(14, 15);
		boolean actual3 = prototype.collidesWith(other);
		
		assertTrue(actual1);
		assertTrue(actual2);
		assertFalse(actual3);
	}

	@Test
	public void testGetLocation() {
		Point expected = new Point(10, 15);
		prototype.setLocation(expected);
		Point actual = prototype.getLocation();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetLocation_null() {
		prototype.setLocation(null);
		Point actual = prototype.getLocation();
		assertNotNull(actual);
	}
	
	@Test
	public void testGetLocation_reference() {
		Point expected = new Point(10, 15);
		prototype.setLocation(expected);
		Point actual = prototype.getLocation();
		assertNotSame(expected, actual);
	}

	@Test
	public void testGetTop() {
		int expected = 10;
		prototype.setY(expected);
		prototype.setLength(4);
		
		prototype.setOrientation(true);
		int actual1 = prototype.getTop();
		prototype.setOrientation(false);
		int actual2 = prototype.getTop();
		
		assertEquals(expected, actual1);
		assertEquals(expected, actual2);
	}

	@Test
	public void testGetBottom() {
		int expected1 = 13;
		int expected2 = 10;
		
		prototype.setY(expected2);
		prototype.setLength(4);
		
		prototype.setOrientation(false);
		int actual1 = prototype.getBottom();
		prototype.setOrientation(true);
		int actual2 = prototype.getBottom();
		
		assertEquals(expected1, actual1);
		assertEquals(expected2, actual2);
	}

	@Test
	public void testGetLeft() {
		int expected = 10;
		prototype.setX(expected);
		prototype.setLength(4);
		
		prototype.setOrientation(true);
		int actual1 = prototype.getLeft();
		prototype.setOrientation(false);
		int actual2 = prototype.getLeft();
		
		assertEquals(expected, actual1);
		assertEquals(expected, actual2);
	}

	@Test
	public void testGetRight() {
		int expected1 = 13;
		int expected2 = 10;
		
		prototype.setX(expected2);
		prototype.setLength(4);
		
		prototype.setOrientation(true);
		int actual1 = prototype.getRight();
		prototype.setOrientation(false);
		int actual2 = prototype.getRight();
		
		assertEquals(expected1, actual1);
		assertEquals(expected2, actual2);
	}

	@Test
	public void testIsHorizontal() {
		prototype.setOrientation(false);
		boolean actual1 = prototype.isHorizontal();
		prototype.setOrientation(true);
		boolean actual2 = prototype.isHorizontal();
		assertFalse(actual1);
		assertTrue(actual2);
	}

	@Test
	public void testGetX() {
		int expected = 10;
		prototype.setX(expected);
		int actual = prototype.getX();
		assertEquals(expected, actual);
	}

	@Test
	public void testGetY() {
		int expected = 10;
		prototype.setY(expected);
		int actual = prototype.getY();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetLength() {
		int expected = 4;
		prototype.setLength(expected);
		int actual = prototype.getLength();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetLength_lessThanOne() {
		prototype.setLength(0);
		int actual1 = prototype.getLength();
		prototype.setLength(-1);
		int actual2 = prototype.getLength();
		assertEquals(1, actual1);
		assertEquals(1, actual2);
	}

	@Test
	public void testGetWidth() {
		int expected1 = 4;
		int expected2 = 1;
		prototype.setLength(expected1);
		
		prototype.setOrientation(true);
		int actual1 = prototype.getWidth();
		prototype.setOrientation(false);
		int actual2 = prototype.getWidth();
		
		assertEquals(expected1, actual1);
		assertEquals(expected2, actual2);
	}

	@Test
	public void testGetHeight() {
		int expected1 = 4;
		int expected2 = 1;
		prototype.setLength(expected1);
		
		prototype.setOrientation(false);
		int actual1 = prototype.getHeight();
		prototype.setOrientation(true);
		int actual2 = prototype.getHeight();
		
		assertEquals(expected1, actual1);
		assertEquals(expected2, actual2);
	}

	@Test
	public void testGetBounds() {
		Rectangle expected1 = new Rectangle(10, 15, 4, 1);
		Rectangle expected2 = new Rectangle(10, 15, 1, 4);
		prototype.setLocation(expected1.x, expected1.y);
		prototype.setLength(expected1.width);
		
		prototype.setOrientation(true);
		Rectangle actual1 = prototype.getBounds();
		prototype.setOrientation(false);
		Rectangle actual2 = prototype.getBounds();
		
		if(actual1 == null || actual2 == null) fail("getBounds returned null.");
		assertEquals(expected1, actual1);
		assertEquals(expected2, actual2);
	}

	@Test
	public void testContains() {
		prototype.setLocation(10, 15);
		prototype.setLength(4);
		
		prototype.setOrientation(true);
		boolean actual1 = prototype.contains(9, 15);
		boolean actual2 = prototype.contains(10, 15);
		boolean actual3 = prototype.contains(13, 15);
		boolean actual4 = prototype.contains(14, 15);
		boolean actual5 = prototype.contains(10, 14);
		boolean actual6 = prototype.contains(10, 16);
		prototype.setOrientation(false);
		boolean actual7 = prototype.contains(10, 14);
		boolean actual8 = prototype.contains(10, 15);
		boolean actual9 = prototype.contains(10, 18);
		boolean actual10 = prototype.contains(10, 19);
		boolean actual11 = prototype.contains(9, 15);
		boolean actual12 = prototype.contains(11, 15);
		
		assertFalse(actual1);
		assertTrue(actual2);
		assertTrue(actual3);
		assertFalse(actual4);
		assertFalse(actual5);
		assertFalse(actual6);
		assertFalse(actual7);
		assertTrue(actual8);
		assertTrue(actual9);
		assertFalse(actual10);
		assertFalse(actual11);
		assertFalse(actual12);
	}
}
